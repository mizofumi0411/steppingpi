package com.company;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizofumi on 2016/06/21.
 */
public class SteppingMoter {

    protected boolean isStop;
    protected List<Pin> pins = new ArrayList<>();
    protected List<Seq> seqs = new ArrayList<>();
    protected int waittime;

    protected int step = 1;

    public SteppingMoter(int step, int waittime) {
        this.step = step;
        this.waittime = waittime;
        pins.add(new Pin(17));
        pins.add(new Pin(22));
        pins.add(new Pin(23));
        pins.add(new Pin(24));
        initialize();
    }

    public SteppingMoter(int step,int waittime,int one,int two,int three,int four){
        this.step = step;
        this.waittime = waittime;
        pins.add(new Pin(one));
        pins.add(new Pin(two));
        pins.add(new Pin(three));
        pins.add(new Pin(four));
        initialize();
    }

    public SteppingMoter(List<Pin> pins,int step, int waittime) {
        //Pinのリストで設定する場合
        this.step = step;
        this.pins = pins;
        this.waittime = waittime;
        initialize();
    }

    protected void initialize(){
        for (Pin pin : pins){
            GPIO.setup(pin.getPin(), GPIO.MODE.OUT);
        }
        seqs.add(new Seq(1, 0, 0, 1));
        seqs.add(new Seq(1, 0, 0, 0));
        seqs.add(new Seq(1, 1, 0, 0));
        seqs.add(new Seq(0, 1, 0, 0));
        seqs.add(new Seq(0, 1, 1, 0));
        seqs.add(new Seq(0, 0, 1, 0));
        seqs.add(new Seq(0, 0, 1, 1));
        seqs.add(new Seq(0, 0, 0, 1));
    }

    public void start(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                int count = 0;
                isStop = false;
                while (!isStop){
                    //
                    // System.out.println("------------------");
                    for (int i = 0; i < 4; i++) {
                        int xpin = pins.get(i).getPin();
                        if (seqs.get(count).getPins().get(i) != 0){
                            //System.out.println(xpin + " True");
                            GPIO.output(xpin,true);
                        }else {
                            //System.out.println(xpin+" False");
                            GPIO.output(xpin,false);
                        }
                    }
                    count += step;

                    if (count >= seqs.size()){
                        count = 0;
                    }
                    if (count < 0){
                        count = seqs.size()+step;
                    }
                    if (waittime != 0){
                        try {
                            Thread.sleep(waittime);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

    public boolean isRunning(){
        return !isStop;
    }

    public void setSpeed(int step,int waittime){
        this.step = step;
        this.waittime = waittime;
    }

    public void stop(){
        isStop = true;
    }

    class Pin{
        int pin;

        public Pin(int pin) {
            this.pin = pin;
        }

        public int getPin() {
            return pin;
        }
    }
}
