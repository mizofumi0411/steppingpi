package com.company;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizofumi on 2016/06/21.
 */
public class Seq {

    List<Integer> pins = new ArrayList<>();

    public Seq(int one, int two, int three, int four) {
        pins.add(one);
        pins.add(two);
        pins.add(three);
        pins.add(four);
    }

    public List<Integer> getPins() {
        return pins;
    }
}
