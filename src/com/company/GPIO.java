package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by mizofumi on 2016/06/21.
 */
public class GPIO {

    public static boolean read(int pin){
        String ret = "";
        boolean result = false;
        try {
            String command = "gpio -g read "+pin;
            Process process = Runtime.getRuntime().exec(command);
            BufferedReader in = new BufferedReader(new InputStreamReader(process
                    .getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                if(line.contains("1")){
                    result = true;
                }
            }
            in.close();
            return result;
        }catch (IOException e){
            return result;
        }
    }
    //ToDo 後で実装する
    public static void output(int pin,boolean tf){
        if (tf){
            try {
                Runtime.getRuntime().exec("gpio -g write "+pin+" 1");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            try {
                Runtime.getRuntime().exec("gpio -g write "+pin+" 0");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void setup(int pin,MODE mode){
        switch (mode){
            case IN:
                try {
                    Runtime.getRuntime().exec("gpio -g mode "+pin+" in");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case OUT:
                try {
                    Runtime.getRuntime().exec("gpio -g mode "+pin+" out");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    enum MODE{
        IN,
        OUT
    }
}
